/*Rafly Maulannasir 301230031 IF 1A*/

#include <iostream>
int main() {
    int n;
    std::cout << "Masukan N : ";
    std::cin >> n;

    for (int i = 0; i < n; i++) {
        int coef = 1;
        for (int j = 0; j < n - i - 1; j++) {
            std::cout << " ";
        }
        for (int j = 0; j <= i; j++) {
            std::cout << coef << " ";
            coef = coef * (i - j) / (j + 1);
        }
        std::cout << std::endl;
    }
    return 0;
}
